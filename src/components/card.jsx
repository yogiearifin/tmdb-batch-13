import { BASE_URL_IMAGE } from "../constants/constant";
import ButtonMovie from "./button";

const Card = ({ ...props }) => {
  const { data, index } = props;
  return (
    <a href={`/detail-movie/${data.id}`} key={index}>
      <div key={index}>
        <img
          src={
            data.gender
              ? `${BASE_URL_IMAGE}${data.profile_path}`
              : data.poster_path
              ? `${BASE_URL_IMAGE}${data.poster_path}`
              : data.title
              ? `https://ui-avatars.com/api/?name=${data.title}`
              : `https://ui-avatars.com/api/?name=${data.name}`
          }
          alt={data.name}
          style={{ maxWidth: "200px" }}
        />
        <h1>{data.title ? data.title : data.name}</h1>
        <ButtonMovie name={"like movie"} />
      </div>
    </a>
  );
};

export default Card;
