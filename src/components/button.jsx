const ButtonMovie = ({ ...props }) => {
  const { name } = props;
  return <button>{name}</button>;
};

export default ButtonMovie;
