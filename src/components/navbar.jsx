import { useDispatch } from "react-redux";
import { searchItem, clearItem } from "../store/actions/movie";

const Navbar = () => {
  const dispatch = useDispatch();

  const searchAll = (e) => {
    if (e.target.value) {
      dispatch(searchItem(e.target.value));
    } else {
      dispatch(clearItem());
    }
  };

  const debounce = (func, timeout = 500) => {
    let timer;
    return (...args) => {
      clearTimeout(timer);
      timer = setTimeout(() => {
        func.apply(this, args);
      }, timeout);
    };
  };

  const debounceSearch = debounce((e) => searchAll(e));

  return (
    <div>
      <a href="/">Logo of our APP </a>
      <h1>This is a Navbar</h1>
      {window.location.pathname === "/" ? (
        <input
          type="text"
          placeholder="search any movie/series/people you want"
          style={{ minWidth: "300px" }}
          onChange={(e) => debounceSearch(e)}
        />
      ) : null}
    </div>
  );
};

export default Navbar;
