export const BASE_URL = "https://api.themoviedb.org/3";
export const BASE_URL_IMAGE = "https://image.tmdb.org/t/p/w500/";
export const API_KEY = process.env.REACT_APP_API_KEY