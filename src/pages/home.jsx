import Card from "../components/card";
import { getMovie } from "../store/actions/movie";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import ButtonMovie from "../components/button";

// const data = [
//   {
//     poster_path:
//       "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ffanart.tv%2Ffanart%2Fmovies%2F497698%2Fmovieposter%2Fblack-widow-5e667a3ea6e0f.jpg&f=1&nofb=1",
//     name: "Black Widow",
//   },
//   {
//     poster_path:
//       "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Ffanart.tv%2Ffanart%2Fmovies%2F497698%2Fmovieposter%2Fblack-widow-5e667a3ea6e0f.jpg&f=1&nofb=1",
//     name: "Widow Black",
//   },
// ];

const Home = () => {
  const dispatch = useDispatch();
  const { movies, loading } = useSelector((state) => state.movies.listMovie);
  const { result, loading: loadingSearch } = useSelector(
    (state) => state.movies.searchResult
  );

  useEffect(() => {
    dispatch(getMovie());
  }, [dispatch]);
  localStorage.setItem("Token", "This Is A Token");
  const token = localStorage.getItem("Token");
  return (
    <div>
      {loading ? (
        "loading ..."
      ) : (
        <>
          {token ? "Anda telah login" : "Anda belum login"}
          <h1>Home</h1>
          {loadingSearch
            ? "Loading Search"
            : result.results && result.results.length
            ? result.results.map((item, index) => {
                return (
                  <div key={index}>
                    <Card data={item} index={index} />
                  </div>
                );
              })
            : movies?.results?.map((item, index) => {
                return (
                  <div key={index}>
                    <Card data={item} index={index} />
                  </div>
                );
              })}
          <ButtonMovie name={"Refresh"} />
          <button
            disabled={movies.page === 1 ? true : false}
            onClick={() => dispatch(getMovie(1))}
          >
            First Page
          </button>
          <button
            disabled={movies.page === 1 ? true : false}
            onClick={() => dispatch(getMovie(movies.page - 1))}
          >
            Previous Page
          </button>
          <button
            disabled={movies.total_pages === movies.page ? true : false}
            onClick={() => dispatch(getMovie(movies.page + 1))}
          >
            Next Page
          </button>
          <button
            disabled={movies.total_pages === movies.page ? true : false}
            onClick={() => dispatch(getMovie(movies.total_pages))}
          >
            Last Page
          </button>
        </>
      )}
    </div>
  );
};

export default Home;
