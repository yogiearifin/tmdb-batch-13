import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { getMovieDetail } from "../store/actions/movie";
import { useParams } from "react-router";
import { BASE_URL_IMAGE } from "../constants/constant";
import StarRatings from "react-star-ratings";

const DetailMovie = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const { detail, loading } = useSelector((state) => state.movies.detailMovie);
  useEffect(() => {
    dispatch(getMovieDetail(id));
  }, [dispatch, id]);
  return (
    <div>
      <h1>Detail Movie</h1>
      {loading ? (
        "Loading..."
      ) : (
        <>
          <h1>{detail.title}</h1>
          <img
            src={`${BASE_URL_IMAGE}${detail.backdrop_path}`}
            alt={detail.title}
          />
          <p>Release Date : {detail.release_date}</p>
          <StarRatings
            rating={detail.vote_average ? detail.vote_average / 2 : 0}
            starRatedColor="yellow"
            numberOfStars={5}
          />
          <p>
            Genre:{" "}
            {detail.genres
              ? detail.genres.map((item, index) => {
                  return (
                    <span key={index}>
                      {detail.genres.length - 1 === index
                        ? item.name
                        : `${item.name}, `}
                    </span>
                  );
                })
              : null}
          </p>
        </>
      )}
    </div>
  );
};

export default DetailMovie;
