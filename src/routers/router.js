import { Switch, Route } from "react-router-dom";
import Home from "../pages/home";
import DetailMovie from "../pages/detailMovie";
import Navbar from "../components/navbar";
import Footer from "../components/footer";

const Routers = () => {
  return (
    <>
      <Navbar />
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/detail-movie/:id">
          <DetailMovie />
        </Route>
        <Route path="*">
          <div>
            <h1>Not Found :(</h1>
          </div>
        </Route>
      </Switch>
      {/* {window.location.pathname !== "/detail-movie" ? <Footer /> : null} */}
      <Footer />
    </>
  );
};

export default Routers;
