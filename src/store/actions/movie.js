import {
  GET_MOVIE_BEGIN,
  GET_MOVIE_DETAIL_BEGIN,
  SEARCH_BEGIN,
  CLEAR,
} from "../../constants/types";

export const getMovie = (page = 1) => {
  return {
    type: GET_MOVIE_BEGIN,
    page,
  };
};

export const getMovieDetail = (id) => {
  return {
    type: GET_MOVIE_DETAIL_BEGIN,
    movie_id: id,
  };
};

export const searchItem = (body) => {
  return {
    type: SEARCH_BEGIN,
    body,
  };
};

export const clearItem = () => {
  return {
    type: CLEAR,
  };
};
