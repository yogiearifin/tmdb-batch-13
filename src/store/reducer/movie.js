import {
  GET_MOVIE_BEGIN,
  GET_MOVIE_SUCCESS,
  GET_MOVIE_FAIL,
  GET_MOVIE_DETAIL_BEGIN,
  GET_MOVIE_DETAIL_SUCCESS,
  GET_MOVIE_DETAIL_FAIL,
  SEARCH_BEGIN,
  SEARCH_SUCCESS,
  SEARCH_FAIL,
  CLEAR,
} from "../../constants/types";

const initialState = {
  listMovie: {
    movies: [],
    loading: false,
    error: null,
  },
  detailMovie: {
    detail: [],
    loading: false,
    error: null,
  },
  searchResult: {
    result: [],
    loading: false,
    error: null,
  },
};

export const movies = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    default:
      return {
        ...state,
      };
    case GET_MOVIE_BEGIN:
      return {
        ...state,
        listMovie: {
          loading: true,
          error: null,
        },
      };
    case GET_MOVIE_SUCCESS:
      return {
        ...state,
        listMovie: {
          movies: payload,
          loading: false,
          error: null,
        },
      };
    case GET_MOVIE_FAIL:
      return {
        ...state,
        listMovie: {
          movies: [],
          loading: false,
          error: error,
        },
      };
    case GET_MOVIE_DETAIL_BEGIN:
      return {
        ...state,
        detailMovie: {
          loading: true,
          error: null,
        },
      };
    case GET_MOVIE_DETAIL_SUCCESS:
      return {
        ...state,
        detailMovie: {
          detail: payload,
          loading: false,
          error: null,
        },
      };
    case GET_MOVIE_DETAIL_FAIL:
      return {
        ...state,
        detailMovie: {
          detail: [],
          loading: false,
          error: error,
        },
      };
    case SEARCH_BEGIN:
      return {
        ...state,
        searchResult: {
          result: [],
          loading: true,
          error: null,
        },
      };
    case SEARCH_SUCCESS:
      return {
        ...state,
        searchResult: {
          result: payload,
          loading: false,
          error: null,
        },
      };
    case SEARCH_FAIL:
      return {
        ...state,
        searchResult: {
          result: [],
          loading: false,
          error: error,
        },
      };
    case CLEAR:
      return {
        ...state,
        searchResult: {
          result: [],
          loading: false,
          error: null,
        },
      };
  }
};
