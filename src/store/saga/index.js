import { all } from "@redux-saga/core/effects";
import { watchGetMovies, watchGetDetailMovie, watchSearch } from "./movie";

export default function* rootSaga() {
  // function generator
  yield all([watchGetMovies(), watchGetDetailMovie(), watchSearch()]);
}
