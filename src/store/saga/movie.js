import axios from "axios";
import {
  GET_MOVIE_BEGIN,
  GET_MOVIE_SUCCESS,
  GET_MOVIE_FAIL,
  GET_MOVIE_DETAIL_BEGIN,
  GET_MOVIE_DETAIL_SUCCESS,
  GET_MOVIE_DETAIL_FAIL,
  SEARCH_BEGIN,
  SEARCH_SUCCESS,
  SEARCH_FAIL,
} from "../../constants/types";
import { put, takeEvery } from "redux-saga/effects";
import { BASE_URL, API_KEY } from "../../constants/constant";

function* getMovies(actions) {
  const { error, page = 1 } = actions;
  try {
    const res = yield axios.get(
      `${BASE_URL}/movie/now_playing?api_key=${API_KEY}&page=${page}`
    );
    yield put({
      type: GET_MOVIE_SUCCESS,
      payload: res.data,
    });
  } catch (err) {
    yield put({
      type: GET_MOVIE_FAIL,
      error: error,
    });
  }
}

function* getMovieDetails(actions) {
  const { error, movie_id } = actions;
  try {
    const res = yield axios.get(
      `${BASE_URL}/movie/${movie_id}?api_key=${API_KEY}`
    );
    yield put({
      type: GET_MOVIE_DETAIL_SUCCESS,
      payload: res.data,
    });
  } catch (err) {
    yield put({
      type: GET_MOVIE_DETAIL_FAIL,
      error: error,
    });
  }
}

function* searchItem(actions) {
  const { error, body } = actions;
  try {
    const res = yield axios.get(
      `${BASE_URL}/search/multi?api_key=${API_KEY}&query=${body}`
    );
    yield put({
      type: SEARCH_SUCCESS,
      payload: res.data,
    });
  } catch (err) {
    yield put({
      type: SEARCH_FAIL,
      error: error,
    });
  }
}

export function* watchGetMovies() {
  yield takeEvery(GET_MOVIE_BEGIN, getMovies);
}

export function* watchGetDetailMovie() {
  yield takeEvery(GET_MOVIE_DETAIL_BEGIN, getMovieDetails);
}

export function* watchSearch() {
  yield takeEvery(SEARCH_BEGIN, searchItem);
}
